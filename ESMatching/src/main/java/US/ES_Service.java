package US;

import US.DB.Persistence.CheckingPostIdEntity;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

public class ES_Service {

    static String INDEX_NAME= "jobid_v2";
    static String TYPE_NAME ="post";

    public static void SearchIDList(String keyword, String parentID, TransportClient client, Session session, int id) throws IOException, ParseException {


        String jobID = "q-";
        jobID += parentID;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        QueryBuilder query = QueryBuilders.boolQuery()
                            .must(QueryBuilders.termQuery("parent_id",jobID))
                            .must(QueryBuilders.matchPhraseQuery("post_explain",keyword))
                            .must(QueryBuilders.rangeQuery("post_date").gte("2019-02-01").lte("2019-08-21"));

        SearchResponse response = client.prepareSearch(INDEX_NAME)
                .setTypes(TYPE_NAME)
                .addSort(FieldSortBuilder.DOC_FIELD_NAME, SortOrder.ASC)
                .setQuery(query)
                .setScroll(new TimeValue(120000))
                .setSize(10000).get();

        do{
            Transaction tx = session.beginTransaction();
            for(SearchHit hit : response.getHits().getHits()){
                Map<String,Object> stringObjectMap = hit.getSourceAsMap();
                CheckingPostIdEntity checkingPostIdEntity = new CheckingPostIdEntity();
                checkingPostIdEntity.setCheckingId(id);
                checkingPostIdEntity.setMatchingKeyword(keyword);
                checkingPostIdEntity.setParentId(parentID);
                checkingPostIdEntity.setPostId((String) stringObjectMap.get("post_id"));
                checkingPostIdEntity.setPostDate(dateFormat.parse(stringObjectMap.get("post_date").toString()));
                session.save(checkingPostIdEntity);
            }
            tx.commit();
            response = client.prepareSearchScroll(response.getScrollId()).setScroll(new TimeValue(120000)).execute().actionGet();
        } while (response.getHits().getHits().length!=0);

        return ;
    }
}
