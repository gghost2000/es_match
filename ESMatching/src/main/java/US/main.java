package US;

import US.DB.Persistence.JobmonsterJobUsaJobMatchingCheckingEntity;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.io.IOException;
import java.net.InetAddress;
import java.text.ParseException;
import java.util.List;

public class main {

    public static void main(String[] args) throws IOException, ParseException {

        Configuration configuration = null;
        SessionFactory sessionFactory = null;
        Session session = null;
        Transaction tx = null;
        configuration = new Configuration();
        sessionFactory = configuration.configure().buildSessionFactory();
        session = sessionFactory.openSession();

        Query query = session.createQuery("from JobmonsterJobUsaJobMatchingCheckingEntity where id>=924");

        List<JobmonsterJobUsaJobMatchingCheckingEntity> entities = query.list();

//        RestHighLevelClient client = new RestHighLevelClient(
//                RestClient.builder(new HttpHost("127.0.0.1",9200,"http"))
//        );
        Settings settings= Settings.builder().put("cluster.name","monster-job").build();

        TransportClient client = new PreBuiltTransportClient(settings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("127.0.0.1"),9300));

        for(JobmonsterJobUsaJobMatchingCheckingEntity entity : entities){
            int id = (int) entity.getId();
            System.out.println(id);
            String parentID = entity.getParentId();
            String keywords = entity.getJobMonsterNmGroup();
            String[] keywordList = keywords.split(",");
            for(String s : keywordList){

                ES_Service.SearchIDList(s,parentID,client,session,id);


            }
        }

        return;
    }
}
