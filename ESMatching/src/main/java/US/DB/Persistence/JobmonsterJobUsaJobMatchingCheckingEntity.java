package US.DB.Persistence;

import javax.persistence.*;

@Entity
@Table(name = "jobmonster_job_usa_job_matching_checking", schema = "crawler_job_monster", catalog = "")
public class JobmonsterJobUsaJobMatchingCheckingEntity {
    private long id;
    private String parentId;
    private Integer jobMonsterId;
    private String jobMonsterNmGroup;
    private Integer usaJobId;
    private String usaJobName;
    private Integer cnt;
    private Integer yn;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Parent_ID", nullable = true, length = 255)
    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "job_monster_id", nullable = true)
    public Integer getJobMonsterId() {
        return jobMonsterId;
    }

    public void setJobMonsterId(Integer jobMonsterId) {
        this.jobMonsterId = jobMonsterId;
    }

    @Basic
    @Column(name = "job_monster_nm_group", nullable = true, length = -1)
    public String getJobMonsterNmGroup() {
        return jobMonsterNmGroup;
    }

    public void setJobMonsterNmGroup(String jobMonsterNmGroup) {
        this.jobMonsterNmGroup = jobMonsterNmGroup;
    }

    @Basic
    @Column(name = "usa_job_id", nullable = true)
    public Integer getUsaJobId() {
        return usaJobId;
    }

    public void setUsaJobId(Integer usaJobId) {
        this.usaJobId = usaJobId;
    }

    @Basic
    @Column(name = "usa_job_name", nullable = true, length = 250)
    public String getUsaJobName() {
        return usaJobName;
    }

    public void setUsaJobName(String usaJobName) {
        this.usaJobName = usaJobName;
    }

    @Basic
    @Column(name = "cnt", nullable = true)
    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    @Basic
    @Column(name = "yn", nullable = true)
    public Integer getYn() {
        return yn;
    }

    public void setYn(Integer yn) {
        this.yn = yn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JobmonsterJobUsaJobMatchingCheckingEntity that = (JobmonsterJobUsaJobMatchingCheckingEntity) o;

        if (id != that.id) return false;
        if (parentId != null ? !parentId.equals(that.parentId) : that.parentId != null) return false;
        if (jobMonsterId != null ? !jobMonsterId.equals(that.jobMonsterId) : that.jobMonsterId != null) return false;
        if (jobMonsterNmGroup != null ? !jobMonsterNmGroup.equals(that.jobMonsterNmGroup) : that.jobMonsterNmGroup != null)
            return false;
        if (usaJobId != null ? !usaJobId.equals(that.usaJobId) : that.usaJobId != null) return false;
        if (usaJobName != null ? !usaJobName.equals(that.usaJobName) : that.usaJobName != null) return false;
        if (cnt != null ? !cnt.equals(that.cnt) : that.cnt != null) return false;
        if (yn != null ? !yn.equals(that.yn) : that.yn != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (jobMonsterId != null ? jobMonsterId.hashCode() : 0);
        result = 31 * result + (jobMonsterNmGroup != null ? jobMonsterNmGroup.hashCode() : 0);
        result = 31 * result + (usaJobId != null ? usaJobId.hashCode() : 0);
        result = 31 * result + (usaJobName != null ? usaJobName.hashCode() : 0);
        result = 31 * result + (cnt != null ? cnt.hashCode() : 0);
        result = 31 * result + (yn != null ? yn.hashCode() : 0);
        return result;
    }
}
