package US.DB.Persistence;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "jobmonster_job_usa_job_matching_checking_post_id", schema = "crawler_job_monster", catalog = "")
public class CheckingPostIdEntity {
    private int id;
    private int checkingId;
    private String matchingKeyword;
    private String parentId;
    private String postId;
    private Date postDate;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "checking_id", nullable = false)
    public int getCheckingId() {
        return checkingId;
    }

    public void setCheckingId(int checkingId) {
        this.checkingId = checkingId;
    }

    @Basic
    @Column(name = "matching_keyword", nullable = false, length = -1)
    public String getMatchingKeyword() {
        return matchingKeyword;
    }

    public void setMatchingKeyword(String matchingKeyword) {
        this.matchingKeyword = matchingKeyword;
    }

    @Basic
    @Column(name = "parent_id", nullable = false, length = -1)
    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "post_id", nullable = false, length = -1)
    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    @Basic
    @Column(name = "post_date", nullable = false)
    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CheckingPostIdEntity that = (CheckingPostIdEntity) o;

        if (id != that.id) return false;
        if (checkingId != that.checkingId) return false;
        if (matchingKeyword != null ? !matchingKeyword.equals(that.matchingKeyword) : that.matchingKeyword != null)
            return false;
        if (parentId != null ? !parentId.equals(that.parentId) : that.parentId != null) return false;
        if (postId != null ? !postId.equals(that.postId) : that.postId != null) return false;
        if (postDate != null ? !postDate.equals(that.postDate) : that.postDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + checkingId;
        result = 31 * result + (matchingKeyword != null ? matchingKeyword.hashCode() : 0);
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (postId != null ? postId.hashCode() : 0);
        result = 31 * result + (postDate != null ? postDate.hashCode() : 0);
        return result;
    }
}
