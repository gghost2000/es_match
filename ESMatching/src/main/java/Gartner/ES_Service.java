package Gartner;

import Gartner.DB.Persistence.GartnerHyperCycleTechEntity;
import Gartner.DB.Persistence.GartnerHyperCycleTechPostEntity;
import US.DB.Persistence.CheckingPostIdEntity;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ES_Service {

    static String INDEX_NAME= "monster*";
    static String TYPE_NAME ="post";

    public  static List<GartnerHyperCycleTechPostEntity> SearchIDList(String keyword, TransportClient client,Session session) throws IOException, ParseException {

        List<GartnerHyperCycleTechPostEntity> entities = new ArrayList<GartnerHyperCycleTechPostEntity>();

        QueryBuilder query = QueryBuilders.boolQuery()
                            .should(QueryBuilders.matchPhraseQuery("post_explain",keyword))
                            .should(QueryBuilders.matchPhraseQuery("post_title",keyword))
                            .minimumShouldMatch(1);

        SearchResponse response = client.prepareSearch(INDEX_NAME)
                .setTypes(TYPE_NAME)
                .addSort(FieldSortBuilder.DOC_FIELD_NAME, SortOrder.ASC)
                .setQuery(query)
                .setScroll(new TimeValue(120000))
                .setSize(10000).get();

        do{
            Transaction tx = session.beginTransaction();
            for(SearchHit hit : response.getHits().getHits()){
                Map<String,Object> stringObjectMap = hit.getSourceAsMap();
                GartnerHyperCycleTechPostEntity gartnerHyperCycleTechPostEntity = new GartnerHyperCycleTechPostEntity();
                gartnerHyperCycleTechPostEntity.setKeyword(keyword);
                gartnerHyperCycleTechPostEntity.setPostId((String) stringObjectMap.get("post_id"));
                gartnerHyperCycleTechPostEntity.setPostTitle((String) stringObjectMap.get("post_title"));
                gartnerHyperCycleTechPostEntity.setPostExplain((String) stringObjectMap.get("post_explain"));
                entities.add(gartnerHyperCycleTechPostEntity);
                session.save(gartnerHyperCycleTechPostEntity);
            }
            tx.commit();
            response = client.prepareSearchScroll(response.getScrollId()).setScroll(new TimeValue(120000)).execute().actionGet();
        } while (response.getHits().getHits().length!=0);

        return entities;
    }
}
