package Gartner.DB.Persistence;

import javax.persistence.*;

@Entity
@Table(name = "gartner_hyper_cycle_tech", schema = "crawler_job_monster", catalog = "")
public class GartnerHyperCycleTechEntity {
    private int id;
    private String tech;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "tech", nullable = true, length = -1)
    public String getTech() {
        return tech;
    }

    public void setTech(String tech) {
        this.tech = tech;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GartnerHyperCycleTechEntity that = (GartnerHyperCycleTechEntity) o;

        if (id != that.id) return false;
        if (tech != null ? !tech.equals(that.tech) : that.tech != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (tech != null ? tech.hashCode() : 0);
        return result;
    }
}
