package Gartner.DB.Persistence;

import javax.persistence.*;

@Entity
@Table(name = "gartner_hyper_cycle_tech_post", schema = "crawler_job_monster", catalog = "")
public class GartnerHyperCycleTechPostEntity {
    private int id;
    private String keyword;
    private String postId;
    private String postTitle;
    private String postExplain;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "keyword", nullable = true, length = 255)
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Basic
    @Column(name = "post_id", nullable = true, length = 255)
    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    @Basic
    @Column(name = "post_title", nullable = true, length = -1)
    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    @Basic
    @Column(name = "post_explain", nullable = true, length = -1)
    public String getPostExplain() {
        return postExplain;
    }

    public void setPostExplain(String postExplain) {
        this.postExplain = postExplain;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GartnerHyperCycleTechPostEntity that = (GartnerHyperCycleTechPostEntity) o;

        if (id != that.id) return false;
        if (keyword != null ? !keyword.equals(that.keyword) : that.keyword != null) return false;
        if (postId != null ? !postId.equals(that.postId) : that.postId != null) return false;
        if (postTitle != null ? !postTitle.equals(that.postTitle) : that.postTitle != null) return false;
        if (postExplain != null ? !postExplain.equals(that.postExplain) : that.postExplain != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (keyword != null ? keyword.hashCode() : 0);
        result = 31 * result + (postId != null ? postId.hashCode() : 0);
        result = 31 * result + (postTitle != null ? postTitle.hashCode() : 0);
        result = 31 * result + (postExplain != null ? postExplain.hashCode() : 0);
        return result;
    }
}
