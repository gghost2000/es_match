package Gartner;

import Gartner.DB.Persistence.GartnerHyperCycleTechEntity;
import Gartner.DB.Persistence.GartnerHyperCycleTechPostEntity;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.List;

public class main {
    public static void main(String[] args) throws IOException, ParseException {
        Configuration configuration = null;
        SessionFactory sessionFactory = null;
        Session session = null;
        Transaction tx = null;
        configuration = new Configuration();
        sessionFactory = configuration.configure().buildSessionFactory();
        session = sessionFactory.openSession();

        Settings settings= Settings.builder().put("cluster.name","monster-job").build();

        TransportClient client = new PreBuiltTransportClient(settings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("127.0.0.1"),9300));

        Query query = session.createQuery("from GartnerHyperCycleTechEntity where id>=1960");

        List<GartnerHyperCycleTechEntity> entities = query.list();

        for (GartnerHyperCycleTechEntity entity : entities){
            int id = entity.getId();
            String keyword = entity.getTech();

            System.out.println(id);
            ES_Service.SearchIDList(keyword,client,session);
        }
        return;
    }
}
